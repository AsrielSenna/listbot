from telegram import (InlineKeyboardButton, InlineKeyboardMarkup, ReplyKeyboardRemove,
                      ReplyKeyboardMarkup, KeyboardButton)
from telegram.ext import ConversationHandler

from cclasses import logger
from data import (ALLO, allo_groups, allo_welcome, allo_valid_type, allo_invalid_type, allo_sent,
                  allos_disabled, intel_asks, creptastes, new_allo, sho_prefix)
from util import user_id_str, is_known_user

def allo(update, context):
    """Handle the beginning of a /allo conversation and propose available allos"""

    if is_known_user(update, context):
        # toggle en cas de désactivation par un admin
        if not context.bot_data["allos_enabled"]:
            update.message.reply_text(allos_disabled)
            return ConversationHandler.END

        user_id = update.effective_user.id
        logger.info("[/allo] User {} asking for a allo".format(update.effective_user.name))

        keyboard = [[KeyboardButton(t)] for t in allo_groups]

        context.bot.send_message(chat_id=user_id, text=allo_welcome,
                                 reply_markup=ReplyKeyboardMarkup(keyboard, one_time_keyboard=True))

        return ALLO.TYPE
    return ConversationHandler.END


def allo_type(update, context):
    """Receive allo type and check correctness"""

    user_id = update.effective_user.id
    al_type = update.message.text[:30].strip()
    context.user_data["allo_type"] = al_type

    if al_type not in allo_groups:
        update.message.reply_text(allo_invalid_type)
        return ALLO.TYPE
    context.bot.send_message(chat_id=user_id,
                             text=allo_valid_type.format(al_type),
                             reply_markup=ReplyKeyboardRemove())
    context.user_data["allo_intel"] = {}
    return allo_gather_intel(update, context)


def allo_gather_intel(update, context):
    """Appelé en boucle quand on connait le type du allo tant qu'on a pas toutes les infos, termine le allo de lui-même"""
    user_id = update.effective_user.id
    al_type = context.user_data["allo_type"]
    if len(allo_groups[al_type].intel) == len(context.user_data["allo_intel"]): # si on n'a pas besoin de plus d'infos
        return allo_send(update, context)
    intel_needed = allo_groups[al_type].intel[len(context.user_data["allo_intel"])] # complexe, détermine la première info qui nous manque
    intel_needed_text = intel_asks[intel_needed]
    # le clavier personnalisé, si il y a un certain nombre d'options possibles
    if intel_needed == ALLO.CREPTASTE:
        intel_reply_markup = ReplyKeyboardMarkup([[KeyboardButton(t)] for t in creptastes], one_time_keyboard=True)
    else:
        intel_reply_markup = ReplyKeyboardRemove()
    # envoi du message pour demander l'info
    context.bot.send_message(chat_id=user_id,
                             text=intel_needed_text,
                             reply_markup=intel_reply_markup)
    return ALLO.INTEL


def allo_intel(update, context):
    """Appelé par le dispatch, enregistre son intel et rend la main à allo_gather_intel"""
    al_type = context.user_data["allo_type"]
    intel_gathered = allo_groups[al_type].intel[len(context.user_data["allo_intel"])] # complexe, détermine la première info qui nous manquait
    context.user_data["allo_intel"][intel_gathered] = intel_asks[intel_gathered] + " " + update.message.text[:3800]
    return allo_gather_intel(update, context)


def allo_send(update, context):
    """Gather allo intels and dispatch to the associated TG group"""

    user_id = update.effective_user.id

    user_data = context.bot_data["users"][user_id]
    al_type = context.user_data["allo_type"]
    mess = "\n".join(context.user_data['allo_intel'].values())

    allo_formatted = new_allo.format(al_type,
                                     update.effective_user.name,
                                     user_id_str(user_data),
                                     mess)

    logger.info("[/allo] {}\n".format(allo_formatted))

    if "Bonjour" in al_type:
        keyboard = []
    else:
        keyboard = [[InlineKeyboardButton("Prendre le allô", callback_data=sho_prefix+"taken")]]
    context.bot.send_message(chat_id=allo_groups[al_type].group_chat_id, # envoi de la notif au groupe concerné
                             text=allo_formatted,
                             reply_markup=InlineKeyboardMarkup(keyboard))

    context.bot.send_message(chat_id=user_id, text=allo_sent, # envoi de la confirmation à l'utilisateur
                             reply_markup = ReplyKeyboardRemove())

    context.user_data["allo_type"] = None
    context.user_data["allo_intel"] = {}

    return ConversationHandler.END

def allo_taken(update, context):
    """Marque le allo comme pris en charge et par qui dans le message qui annonçait le allo"""

    user_id = update.effective_user.id
    user_at = update.effective_user.name
    query = update.callback_query

    logger.info("[/allo] {} has taken a allo".format(user_id))
    query.edit_message_text(update.effective_message.text + "\n\nPris par {}".format(user_at),
                            reply_markup=InlineKeyboardMarkup([]))
