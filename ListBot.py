import os
import sys
from threading import Thread

import telegram
from telegram import ReplyKeyboardRemove, InlineKeyboardMarkup
from telegram.ext import (CallbackQueryHandler, CommandHandler, ConversationHandler, Filters,
                          MessageHandler, PicklePersistence)

from cclasses import tgbot, logger
import config
from data import ALLO, BROADCAST, CLEAR, HELLO, success_cancel, access_denied, validation_subscribe
from allo import allo, allo_type, allo_intel, allo_intel_loc, allo_intel_contact, allo_taken
from hello import start, takeover, update_id, pseudo, major, first_name, last_name
from registration import reg_button # , register
from enigms import answer # , enigms, next_enigm, stats
from admin import (broadcast, channel, message, confirmation, bdc_button1, bdc_button2,
                   set_user_data, list_command, list_users, set_day, next_day, # list_mysteirb,
                   elevate, demote, echo, photoecho, audioecho, videoecho, # registration_state,
                   docecho, locecho, contactecho)
# from mysteirb import MYSTEIRB_SUB, mysteirb, mysteirb_sub
from simple_commands import (ping, subscribe, sub_button, HELP_MESSAGE, ask_help, ask_help_message,
                             team, musik, video, revisionneirb) # activities_help,
from util import has_clearance

persistence = PicklePersistence(filename='bot_data_pickle')


def button(update, context):
    """Handle user interaction on any button and dispatch accordingly"""

    query = update.callback_query
    payload = query.data
    query.answer() # avoid hogging the client

    if payload == "end":
        query.edit_message_text(validation_subscribe, reply_markup=InlineKeyboardMarkup([]))
    elif payload.startswith("sub_"):
        sub_button(update, context)
    elif payload.startswith("reg_"):
        reg_button(update, context)
    elif payload.startswith("bdc_"):
        return bdc_button1(update, context)
    elif payload.startswith("ust_"):
        return bdc_button2(update, context)
    elif payload.startswith("sho_"):
        allo_taken(update, context)


def cancel(update, context):
    """Cancel current Conversation"""
    update.message.reply_text(success_cancel, reply_markup=ReplyKeyboardRemove())
    return ConversationHandler.END


def stop_and_restart():
    """Gracefully stop the Updater and replace the current process with a new one"""
    updater.stop()
    os.execl(sys.executable, *sys.argv, *sys.argv)

def reboot(update, context):
    """Triggers a cold reboot from the command line (quite magic)"""
    user_id = update.effective_user.id
    if has_clearance(user_id, CLEAR.ADMIN, context):
        logger.info("[/reboot] Rebooting...")
        update.message.reply_text('Bot is restarting...')
        Thread(target=stop_and_restart).start()
    elif context.bot_data["verbose_access_denied"]:
        update.message.reply_text(access_denied)


updater = telegram.ext.updater.Updater(bot=tgbot, persistence=persistence, use_context=True)
dispatcher = updater.dispatcher

for key, value in config.initial_bot_data.items():
    dispatcher.bot_data.setdefault(key, value)

cancel_handler = CommandHandler("cancel", cancel)

start_handler = ConversationHandler(
    entry_points = [CommandHandler("start", start),
                    CommandHandler("start_again", update_id)],
    states = {HELLO.FIRST_NAME: [cancel_handler, MessageHandler(Filters.text, first_name)],
              HELLO.TAKEOVER:   [cancel_handler, MessageHandler(Filters.text, takeover)],
              HELLO.LAST_NAME:  [cancel_handler, MessageHandler(Filters.text, last_name)],
              HELLO.MAJOR:      [cancel_handler, MessageHandler(Filters.text, major)],
              HELLO.PSEUDO:     [cancel_handler, MessageHandler(Filters.text, pseudo)]},
    fallbacks = [cancel_handler],
    name = "start_handler",
    persistent = True)

dispatcher.add_handler(start_handler)


broadcast_handler = ConversationHandler(
    entry_points = [CommandHandler("broadcast", broadcast)],
    states = {BROADCAST.CHANNEL:      [cancel_handler,
                                       MessageHandler(Filters.text, channel),
                                       CallbackQueryHandler(button)],
              BROADCAST.USERS_SELECT: [cancel_handler, CallbackQueryHandler(button)],
              BROADCAST.MESSAGE:      [cancel_handler, MessageHandler(Filters.text, message)],
              BROADCAST.CONFIRMATION: [cancel_handler, MessageHandler(Filters.text, confirmation)]},
    fallbacks = [cancel_handler],
    name = "broadcast_handler",
    persistent = True)

dispatcher.add_handler(broadcast_handler)


allo_handler = ConversationHandler(
    entry_points = [CommandHandler("allo", allo)],
    states = {ALLO.TYPE:  [cancel_handler, MessageHandler(Filters.text, allo_type)],
              ALLO.INTEL: [cancel_handler,
                           MessageHandler(Filters.text, allo_intel)]},
    fallbacks = [cancel_handler],
    name = "allo_handler",
    persistent = True)

dispatcher.add_handler(allo_handler)


help_handler = ConversationHandler(
    entry_points = [CommandHandler("help", ask_help)],
    states = {HELP_MESSAGE: [cancel_handler, MessageHandler(Filters.text, ask_help_message)]},
    fallbacks = [cancel_handler],
    name = "help_handler",
    persistent = True)

dispatcher.add_handler(help_handler)


# mysteirb_handler = ConversationHandler(
#     entry_points = [CommandHandler("mysteirb", mysteirb)],
#     states = {MYSTEIRB_SUB: [cancel_handler, MessageHandler(Filters.text, mysteirb_sub)]},
#     fallbacks = [cancel_handler],
#     name = "mysteirb_handler",
#     persistent = True)

# dispatcher.add_handler(mysteirb_handler)

dispatcher.add_handler(CallbackQueryHandler(button))

dispatcher.add_handler(CommandHandler("subscribe", subscribe))
# dispatcher.add_handler(CommandHandler("register", register))
# dispatcher.add_handler(CommandHandler("activities", activities_help))

# dispatcher.add_handler(CommandHandler("enigms", enigms))
# dispatcher.add_handler(CommandHandler("next_enigm", next_enigm))
# dispatcher.add_handler(CommandHandler("stats", stats))

dispatcher.add_handler(CommandHandler("team", team))
dispatcher.add_handler(CommandHandler("musik", musik))
dispatcher.add_handler(CommandHandler("video", video))
dispatcher.add_handler(CommandHandler("revisionneirb", revisionneirb))

dispatcher.add_handler(CommandHandler("ping", ping))
dispatcher.add_handler(MessageHandler(Filters.text & (~Filters.command), answer))

dispatcher.add_handler(CommandHandler("list", list_command))

# staff
dispatcher.add_handler(CommandHandler("list_users", list_users))
# dispatcher.add_handler(CommandHandler("list_mysteirb", list_mysteirb))

# admin
dispatcher.add_handler(CommandHandler("set_day", set_day))
dispatcher.add_handler(CommandHandler("next_day", next_day))
dispatcher.add_handler(CommandHandler("elevate", elevate))
dispatcher.add_handler(CommandHandler("demote", demote))
# dispatcher.add_handler(CommandHandler("registration_state", registration_state))
dispatcher.add_handler(CommandHandler("set_user_data", set_user_data))
dispatcher.add_handler(CommandHandler('reboot', reboot))

dispatcher.add_handler(CommandHandler("echo", echo))
dispatcher.add_handler(MessageHandler(Filters.photo, photoecho))
dispatcher.add_handler(MessageHandler(Filters.audio, audioecho))
dispatcher.add_handler(MessageHandler(Filters.video, videoecho))
dispatcher.add_handler(MessageHandler(Filters.document, docecho))
dispatcher.add_handler(MessageHandler(Filters.location, locecho))
dispatcher.add_handler(MessageHandler(Filters.contact, contactecho))

updater.start_polling()
updater.idle()
