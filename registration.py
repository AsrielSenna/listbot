from telegram import InlineKeyboardMarkup

from cclasses import logger
from data import activities, reg_prefix, register_question
from util import generate_inline_buttons, is_known_user

def register(update, context):
    """Handle /register command and display today's activities"""

    if is_known_user(update, context):
        user_id = update.effective_user.id
        user_regs = context.bot_data["users"][user_id]["registrations"]

        if context.bot_data["registrations_open"]:
            day = context.bot_data["date"]
            activities_today = activities[day]

            keyboard = generate_inline_buttons([(act["id"],
                                                 act["time"].strftime("%Hh%M")+" + "+act["name"])
                                                 for act in activities_today],
                                               (lambda id: id in user_regs),
                                               reg_prefix)

            update.message.reply_text(register_question,
                                      reply_markup=InlineKeyboardMarkup(keyboard))

        else:
            update.message.reply_text("Les inscriptions sont fermées pour aujourd'hui 😥")


def reg_button(update, context):
    """Handle user interaction on registration buttons"""

    user_id = update.effective_user.id
    query = update.callback_query
    choice = query.data[4:]

    user_regs = context.bot_data["users"][user_id]["registrations"]

    if choice in user_regs:
        user_regs.remove(choice)
    else:
        user_regs.append(choice)

    day = context.bot_data["date"]
    activities_today = activities[day]

    keyboard = generate_inline_buttons([(act["id"],
                                         act["time"].strftime("%Hh%M")+" + "+act["name"])
                                         for act in activities_today],
                                       (lambda id: id in user_regs),
                                       reg_prefix)

    logger.info(f"[/register] {update.effective_user.id} registrations updated : {user_regs}")
    query.edit_message_text(register_question, reply_markup=InlineKeyboardMarkup(keyboard))
