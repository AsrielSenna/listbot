# ListBot
Un bot Telegram créé pour des campagnes électorales

Construit à partir de SDAV_Bot, créé avec talent par FabAlchemy pour la Semaine des Arts Virtuelle (Avril 2020) 💜💛

### Adapter le bot aux besoins de la liste et au contexte de déploiement du bot
Les champs à customiser pour adapter le bot au contexte d'utilisation se trouvent principalement dans data.py (les `fun_answers`, les noms des activités et le planning, et les autres champs de texte nécessaires au traitement des commandes)
Il faut aussi changer la commande /nomdelaliste (alias /revisionneirb pour les tests) en lui mettant le vrai nom de la liste.

### Pour démarrer le bot pour la première fois :
1. enregister le bot_token fourni par BotFather dans le fichier BOT_TOKEN
2. supprimer le fichier bot_data_pickle si il y en a un
3. lancer ListBot.py avec python3
4. se connecter au bot avec Telegram, lancer /start
5. prendre le contrôle quand le bot le propose
6. si quelqu'un d'autre a pris le contrôle et que le bot ne vous le propose pas, fermer python3 et recommencer à l'étape 2

Ensuite, il faut remplir un certain nombre de valeurs, pas toutes obligatoires, pour le bon fonctionnement du bot :
- les chats où on enverra les logs, dans la variable `log_chat_ids` (optionnel, les logs apparaissent aussi dans la console)
- le chat où envoyer les messages /help, dans la variable `assist_chat`
- enregistrer un `owner` et un `staff` en dur (conseillé, mais le contrôle peut être pris autrement)
- régler `time_origin` avec la date de début des campagnes (normalement obligatoire, mais pas d'implémentation pour l'instant)
- personnaliser `team_prez` avec du texte, et `team_pic`, `team_musik` et `team_video` avec des media_id obtenus via `photoecho`, `audioecho` et `videoecho` (nécessaire, ou alors désactiver les commandes correspondantes)
- donner la liste des commandes publiques à BotFather pour qu'elles soient simplement accessibles par les utilisateurs

### Les commandes
Liste des commandes publiques
- celles qui marchent (à fournir à BotFather) :
  - /start
    - s'enregistrer en tant que nouvel utilisateur
  - /start_again
    - changer ses infos
  - /team
    - afficher les membres de la liste et la photo de groupe
  - /musik
    - recevoir la musique de campagne de la liste
  - /video
    - recevoir la vidéo de campagne de la liste
  - /nomdelaliste (/revisionneirb pour la démo)
    - recevoir les trois précédents
  - /allo
    - commander un allô
  - /help
    - demander de l'aide
  - /ping
    - tester si le bot est en ligne
  - /list
    - obtenir une liste des activités du jour
- celles qui seront retirées ou qui ne marchent pas (trop spécifiques à la SDAV)
  - /mysteirb
    - faire un guess sur le vrai nom de la liste
  - /subscribe
    - choisir ses préférences de notifications
  - /register
    - s'inscrire dans des activités
  - /activities
    - avoir des infos sur le fonctionnement des activités
  - /enigms
    - avoir des infos sur le fonctionnement des énigmes
  - /next_enigm
    - connaitre la prochaine énigme
  - /stats
    - afficher le leaderboard des énigmes

Liste des commandes staff- ou admin-only (ne pas fournir à BotFather)
- /broadcast
  - (staff) envoyer un message à des utilisateurs du bot
- /set_day [day]
  - (admin) forcer le numéro du jour
- /next_day
  - (admin) passer au jour suivant (à trigger automatiquement)
- /elevate [userid]
  - (admin) ajouter un utilisateur au staff
- /demote [userid]
  - (admin) retirer un utilisateur du staff
- /list_users
  - (staff) lister tous les utilisateurs enregistrés
- /registration_state
  - (admin) ouvre ou ferme les inscriptions à des activités (à trigger automatiquement)
- /set_user_data [userid] [field] [value]
  - (admin) change la valeur d'un champ d'un utilisateur
- /echo
  - afficher des infos dans la console, pas de retour utilisateur, ouvert à tous mais ne pas publier car inutile
- /list_mysteirb
  - (staff) affiche les réponses des gens au /mysteirb, va avec /mysteirb
- /reboot
  - (admin) reboot le client et recharge le code
