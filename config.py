# chat_id des groupes et/ou canaux où poster et/ou des personnes à qui envoyer les logs
log_chat_ids = {-422973032, -1001303061818}
# pour un groupe, envoyer un /echo depuis le groupe, le log indiquera le chat_id du groupe d'où vient le message
# pour un canal, ouvrir telegram web, le chat_id est -100 suivi de ce qu'il y a dans l'url entre le c et le _
# pour une personne, c'est simplement son user_id

assist_chat = -739026243 # il faut en mettre un, c'est la cible du /help

# gestion des autorisations
# si on veut hardcoder l'owner et le staff mais c'est pas nécessaire
owner = None
staff = [] # attention, un staff enregistré ici ne peut pas être viré avec /demote
creator = 685668703


wide_broadcast_enabled = False # si on permet l'envoi d'un message à tous les utilisateurs sans exception
# désactivé parce que normalement le canal officiel est fait pour ça


initial_bot_data = {"allos_enabled" : True,
                    "verbose_access_denied" : False}
