from telegram import InlineKeyboardButton
from data import subscribe_validation, not_known_user, CLEAR
import config

def user_id_str(user_data):
    """Pretty print a user's information"""
    return "{} {} ({}) {}".format(user_data["first_name"],
                                  user_data["last_name"],
                                  user_data["pseudo"],
                                  (user_data["major"][-1] if user_data["major"] else ""))

def get_user_from_id(bot, userid):
    return bot.get_chat_member(userid, userid).user

def generate_inline_buttons(liz, active, prefix):
    """
    liz : liste de (id, text)
    active : lambda qui prend un id et renvoie si l'option doit être active ou non
    prefix : trois caractères suivis d'un _, identifie là d'où on vient
    """
    keyboard = []
    for but_id, text in liz:
        keyboard.append([InlineKeyboardButton(text + " " + ("✅" if active(but_id) else "❌"),
                                              callback_data=prefix+but_id)])
    keyboard.append([InlineKeyboardButton(subscribe_validation, callback_data="end")])
    return keyboard

def is_known_user(update, context):
    user_id = update.effective_user.id
    if user_id in context.bot_data["users"]:
        return True
    context.bot.send_message(chat_id=user_id, text=not_known_user)
    return False

def has_clearance(userid, clearlevel, context):
    """Retourne si oui ou non l'utilisateur spécifié a le niveau d'accréditation clearlevel"""
    return userid in (config.creator, config.owner)\
           or (not clearlevel)\
           or (clearlevel <= context.bot_data["users"][userid]["clearance"])\
           or ((userid in config.staff) and (clearlevel <= CLEAR.STAFF))
