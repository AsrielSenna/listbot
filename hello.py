from copy import deepcopy as _deepcopy
from pprint import pformat
from datetime import date

from telegram.ext import ConversationHandler
from telegram import KeyboardButton, ReplyKeyboardMarkup, ReplyKeyboardRemove

from cclasses import logger
from data import (HELLO, CLEAR, empty_user, already_started, intro, ask,
                  empty_enigm_tracker, incorrect_yes_no, ask_data, inform_cancel, invalid_input,
                  majors, recap_data, incorrect_data, finish_start)
from util import user_id_str, has_clearance

def start(update, context):
    """Register the user in the database"""

    chat_id = update.effective_chat.id
    user_id = update.effective_user.id

    # Handle mulitple /start and the very first user
    if "users" in context.bot_data:
        if user_id not in context.bot_data["users"]:
            logger.info("[/start] New user registering")
            context.bot_data["users"][user_id] = _deepcopy(empty_user)
        else:
            context.bot.send_message(chat_id=chat_id, text=already_started)
            return ConversationHandler.END
    else: # the first user issued /start, we init the server "database"
        logger.info("[/start] First user registered")
        context.bot_data["users"] = {user_id: _deepcopy(empty_user)}
        context.bot_data["date"] = date.today().day
        context.bot_data["registrations_open"] = True

    # Send greetings
    context.bot.send_message(chat_id=chat_id, text=intro)

    # prise de contrôle si le bot n'a pas de propriétaire
    if (not "owner" in context.bot_data) or (not context.bot_data["owner"]):
        context.bot_data["owner"] = None
        for userid in context.bot_data["users"]:
            if has_clearance(userid, CLEAR.ADMIN, context):
                context.bot_data["owner"] = userid
                break
    if not context.bot_data["owner"]:
        context.bot.send_message(chat_id=chat_id, text=ask[HELLO.TAKEOVER])
        return HELLO.TAKEOVER

    # ask for the pseudo
    context.user_data["enigms"] = _deepcopy(empty_enigm_tracker)
    return update_id(update, context, first_time=True)


def takeover(update, context):
    """acknowledge the decision whether or not to takeover the bot and resume registering"""

    chat_id = update.effective_chat.id
    user_id = update.effective_user.id

    answer = update.message.text.strip().lower()

    if answer in ("o", "oui"):
        context.bot_data["users"][user_id]["clearance"] = CLEAR.ADMIN
        context.bot_data["owner"] = user_id
        logger.info(f"[takeover] {user_id} {update.effective_user.name} has taken owner control of the bot")
        context.bot.send_message(chat_id=chat_id, text="""Vous avez pris le contrôle de ce bot avec succès.
Vous êtes maintenant son propriétaire et administrateur suprème.\n
Félicitations 😎""")
    elif answer in ("n", "non"):
        logger.info(f"[takeover] {user_id} {update.effective_user.name} has declined takeover")
        context.bot.send_message(chat_id=chat_id, text="Tu fais preuve d'une grande sagesse.")
    else:
        context.bot.send_message(chat_id=chat_id, text=incorrect_yes_no)
        return HELLO.TAKEOVER

    # ask for the pseudo
    return update_id(update, context, first_time=True)


def update_id(update, context, first_time=False):
    """Handle the /start_again command"""
    update.message.reply_text(ask_data + ("\n"+inform_cancel if not first_time else ""))
    update.message.reply_text(ask[HELLO.PSEUDO])
    return HELLO.PSEUDO


def pseudo(update, context):
    """Réception du pseudo, demande de la filière avec un clavier personnalisé"""

    user_id = update.effective_user.id
    user_input = update.message.text[:30].strip().replace("\n", " ")

    for userid in context.bot_data["users"]:
        if user_input == context.bot_data["users"][userid]["pseudo"] and not userid == user_id:
            update.message.reply_text(invalid_input + " (ce pseudo est déjà utilisé)")
            return HELLO.PSEUDO
    if user_input == "":
        update.message.reply_text(invalid_input)
        return HELLO.PSEUDO

    context.bot_data["users"][user_id]["pseudo"] = user_input
    keyboard = [[KeyboardButton(maj)] for maj in majors]
    update.message.reply_text(ask[HELLO.MAJOR],
                              reply_markup=ReplyKeyboardMarkup(keyboard, one_time_keyboard=True))
    return HELLO.MAJOR


def major(update, context):
    """Réception de la filière, demande du prénom"""

    user_id = update.effective_user.id
    user_input = update.message.text[:30].strip()

    if user_input not in majors:
        update.message.reply_text(invalid_input)
        return HELLO.MAJOR

    context.bot_data["users"][user_id]["major"] = user_input
    update.message.reply_text(ask[HELLO.FIRST_NAME], reply_markup=ReplyKeyboardRemove())
    return HELLO.FIRST_NAME


def first_name(update, context):
    """Réception du prénom, demande du nom de famille"""

    user_id = update.effective_user.id
    user_input = update.message.text[:30].strip().replace("\n", " ")

    if user_input == "":
        update.message.reply_text(invalid_input)
        return HELLO.FIRST_NAME

    context.bot_data["users"][user_id]["first_name"] = user_input
    update.message.reply_text(ask[HELLO.LAST_NAME])

    return HELLO.LAST_NAME


def last_name(update, context):
    """Réception du nom de famille, fin de conversation"""

    user_id = update.effective_user.id
    user_input = update.message.text[:30].strip().replace("\n", " ")

    if user_input == "":
        update.message.reply_text(invalid_input)
        return HELLO.LAST_NAME


    context.bot_data["users"][user_id]["last_name"] = user_input
    update.message.reply_text(ask[HELLO.END])

    user_str = user_id_str(context.bot_data["users"][user_id])
    update.message.reply_text(recap_data + " \n" + user_str + "\n" + incorrect_data)

    update.message.reply_text(finish_start)

    logger.info(f"[/start] User {user_id} updated its data : {user_str}")
    logger.info("[/start] Current users :")
    logger.info(pformat(context.bot_data["users"]))
    return ConversationHandler.END
