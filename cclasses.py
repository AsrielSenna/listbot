import logging
import telegram.bot
from telegram.utils.request import Request
from telegram.ext import messagequeue as mq

import config
from tokresolve import tokfilename

def _read_token():
    with open(tokfilename or 'BOT_TOKEN') as tokfile:
        return tokfile.read()[:-1]

token = _read_token() # commenter et dupliquer cette ligne pour tester sur un autre bot telegram


# Slightly modify the Bot class to use a MessageQueue in order to avoid
# Telegram's flood limits (30 msg/sec)
class MQBot(telegram.bot.Bot):
    """A subclass of Bot which delegates send method handling to MessageQueue"""
    def __init__(self, *args, is_queued_def=True, mqueue=None, **kwargs):
        super().__init__(*args, **kwargs)
        self._is_messages_queued_default = is_queued_def
        self._msg_queue = mqueue or mq.MessageQueue()

    def __del__(self):
        try:
            self._msg_queue.stop()
        except:
            pass

    @mq.queuedmessage
    def send_message(self, *args, **kwargs):
        """Wrapped method would accept new `queued` and `isgroup` OPTIONAL arguments"""
        return super().send_message(*args, **kwargs)


# Limit global throughput to 25 messages per second
q = mq.MessageQueue(all_burst_limit=25, all_time_limit_ms=1000)
# Set connection pool size for bot
request = Request(con_pool_size=8)

tgbot = MQBot(token, request=request, mqueue=q)


logging.basicConfig(format='%(asctime)s - %(message)s', level=logging.INFO)
class TelegramLogger(logging.Logger):
    """Pour envoyer les logs par message en plus de les envoyer dans la console"""
    def __init__(self, name):
        super().__init__(name)

    def info(self, msg, *args, **kwargs):
        for lci in config.log_chat_ids:
            tgbot.send_message(lci, msg)
        super().info(msg, *args, **kwargs)

logging.setLoggerClass(TelegramLogger)
logger = logging.getLogger(__name__)
