import random

from telegram import InlineKeyboardMarkup, ParseMode
from telegram.ext import ConversationHandler

from cclasses import logger
import config
from data import (fun_answers, subscriptions, sub_prefix, ask, HELLO, tutos,
                  team_pic, team_prez, team_musik, team_video)
from util import user_id_str, generate_inline_buttons

def ping(update, context):
    """Handle the /ping command"""
    context.bot.send_message(chat_id=update.effective_chat.id, text=random.choice(fun_answers))

def subscribe(update, context):
    """Handle the /subscribe command"""

    chat_id = update.effective_chat.id
    user_id = update.effective_user.id
    user_subs = context.bot_data["users"][user_id]["subscriptions"]

    keyboard = generate_inline_buttons(list(subscriptions.items()),
                                       (lambda id: id in user_subs),
                                       sub_prefix)

    context.bot.send_message(chat_id=chat_id, text=ask[HELLO.SUBSCRIBE],
                             reply_markup=InlineKeyboardMarkup(keyboard))


def sub_button(update, context):
    """Handle user interaction on subscription buttons"""

    user_id = update.effective_user.id
    query = update.callback_query
    choice = query.data[4:]

    user_subs = context.bot_data["users"][user_id]["subscriptions"]

    if choice in user_subs:
        user_subs.remove(choice)
    else:
        user_subs.append(choice)

    keyboard = generate_inline_buttons(list(subscriptions.items()),
                                       (lambda id: id in user_subs),
                                       sub_prefix)

    logger.info(f"[/subscribe] {update.effective_user.id} subscriptions updated : {user_subs}")
    query.edit_message_text(ask[HELLO.SUBSCRIBE], reply_markup=InlineKeyboardMarkup(keyboard))

def activities_help(update, context):
    """Handle /activities and send the links"""

    chat_id = update.effective_chat.id
    context.bot.send_message(chat_id=chat_id, text=tutos.format(context.bot.username))


HELP_MESSAGE = 42

def ask_help(update, context):
    """Handle the /help command and ask for the problem"""

    update.message.reply_text("""Désolé de te causer des problèmes... 😓
Dis-moi ce qu'il se passe exactement ?""")

    return HELP_MESSAGE


def ask_help_message(update, context):
    """Transfer help message to the correct channel"""

    user_id = update.effective_user.id

    user_repr = update.effective_user.name
    if user_id in context.bot_data["users"]:
        user_repr += ", "+user_id_str(context.bot_data["users"][user_id])

    formatted_help_msg = f"{user_repr} a un problème avec moi 😅\n{update.message.text[:3800]}"
    logger.info(f"[/help] {formatted_help_msg}")
    context.bot.send_message(chat_id=config.assist_chat, text=formatted_help_msg)
    update.message.reply_text("J'ai transmis tes doléances")

    return ConversationHandler.END


def team(update, context):
    """Envoie une liste des membres de la liste, avec le rôle et le @ de chacun"""

    chat_id = update.effective_chat.id

    context.bot.send_photo(chat_id, team_pic)
    context.bot.send_message(chat_id,
                             team_prez,
                             parse_mode=ParseMode.HTML)

def musik(update, context):
    """Envoie la musique de la choré de la liste"""

    chat_id = update.effective_chat.id

    context.bot.send_audio(chat_id, team_musik)

def video(update, context):
    """Envoie la vidéo de la liste"""

    chat_id = update.effective_chat.id

    context.bot.send_video(chat_id, team_video)

def revisionneirb(update, context):
    """Envoie la vidéo, la musique et la liste des membres de la liste"""

    video(update, context)
    musik(update, context)
    team(update, context)
