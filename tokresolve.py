import sys

liz = [arg for arg in sys.argv if arg.lower().startswith(('tokfile=', '--tokfile=', '-t='))]
if len(liz) > 1:
    raise ValueError("Only one tokfile argument must be given")
tokfilename = liz[0].partition('=')[2] if liz else sys.argv[1] if len(sys.argv)>1 else None
del liz

if __name__ == '__main__':
    print(tokfilename)
