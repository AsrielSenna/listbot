from pprint import pformat
from telegram.ext import ConversationHandler
from telegram import InlineKeyboardButton, InlineKeyboardMarkup

from cclasses import logger
import config
from data import (CLEAR, access_denied, user_not_found, BROADCAST, elevated_success,
                  incorrect_yes_no, broadcast_str, right_channel, wrong_channel, wrong_message,
                  bdc_prefix1, bdc_prefix2, activities, subscriptions, update_day_finished,
                  next_day_dailies, next_day_enigms, success_cancel, select_these_users)
from util import user_id_str, get_user_from_id, has_clearance

def set_day(update, context):
    """Set current day manually"""

    user_id = update.effective_user.id

    if has_clearance(user_id, CLEAR.ADMIN, context):
        try:
            day = int(context.args[0])
        except (IndexError, ValueError):
            update.message.reply_text("Syntaxe : /set_day [day]")
        else:
            context.bot_data["date"] = day
            update.message.reply_text(update_day_finished.format(context.bot_data["date"]))

    elif context.bot_data["verbose_access_denied"]:
        update.message.reply_text(access_denied)


def next_day(update, context):
    """Move on to the next day and alert users subscribed to `dailies` and `enigms`"""

    user_id = update.effective_user.id

    if has_clearance(user_id, CLEAR.ADMIN, context):
        context.bot_data["date"] = context.bot_data["date"] + 1

        users = context.bot_data["users"]
        for usr in users:
            if "dailies" in users[usr]["subscriptions"]:
                context.bot.send_message(chat_id=usr, text=next_day_dailies)
            if "enigms" in users[usr]["subscriptions"]:
                context.bot.send_message(chat_id=usr, text=next_day_enigms)

        update.message.reply_text(update_day_finished.format(context.bot_data["date"]))

    elif context.bot_data["verbose_access_denied"]:
        update.message.reply_text(access_denied)


def list_mysteirb(update, context):
    """Liste les utilisateurs qui ont fait un guess avec /mysteirb"""

    user_id = update.effective_user.id
    chat_id = update.effective_chat.id

    if has_clearance(user_id, CLEAR.STAFF, context):
        users = context.bot_data["users"]

        msg = ""
        for rank, userid in enumerate(users):
            if "mysteirb" in users[userid]:
                msg += "- " + users[userid]["mysteirb"] + " : " + user_id_str(users[userid]) + "\n"

            if rank % 10 == 9:
                context.bot.send_message(chat_id=chat_id, text=msg)
                msg = ""

        if msg:
            context.bot.send_message(chat_id=chat_id, text=msg)

    elif context.bot_data["verbose_access_denied"]:
        update.message.reply_text(access_denied)


def set_user_data(update, context):
    """Force user data to a certain value."""

    user_id = update.effective_user.id

    if has_clearance(user_id, CLEAR.ADMIN, context):
        target_user = int(context.args[0])
        target_key = context.args[1]
        target_value = context.args[2]

        users = context.bot_data["users"]

        if target_user in users:
            # if target_key == "time":
            #     if int(target_value) in users:
            #         users[target_user]["enigm_data"]["time"] = users[int(target_value)]["enigm_data"]["time"]
            #         update.message.reply_text("Done")
            # servait à punir les tricheurs des énigmes
            #
            # elif target_key in ['first_name', 'last_name', 'pseudo']:
            if target_key in users[target_user] and target_key not in ("clearance", "subscriptions", "registrations", "enigm_data"):
                users[target_user][target_key] = target_value
                update.message.reply_text("Done")

            else:
                update.message.reply_text("Bad target_key")
        else:
            update.message.reply_text(user_not_found.format(str(target_user)))

    elif context.bot_data["verbose_access_denied"]:
        update.message.reply_text(access_denied)


def registration_state(update, context):
    """Switch registration_state for everyone"""

    user_id = update.effective_user.id
    if has_clearance(user_id, CLEAR.ADMIN, context):
        context.bot_data["registrations_open"] = (not context.bot_data["registrations_open"])

        logger.info(f"Changed registrations state : {context.bot_data['registrations_open']}")
        update.message.reply_text(f"Etat des inscriptions changé : {context.bot_data['registrations_open']}")

    elif context.bot_data["verbose_access_denied"]:
        update.message.reply_text(access_denied)


def echo(update, context):
    """Display the user informations in the console"""

    chat_id = update.effective_chat.id
    user = update.effective_user

    logger.info(f"[/echo] user {user.name} ({user.id}) issued a /echo command (chat: {chat_id})")

def photoecho(update, context):
    """Displays infos about a received photo in the console, no user feedback"""

    user_id = update.effective_user.id

    logger.info(f"[photoecho] user {user_id} sent a picture, which characteristics are :")
    # fsiz = 0
    for photosiz in update.message.effective_attachment:
        logger.info("file size : %s", photosiz.file_size)
        logger.info(f"file id : {photosiz.file_id}")
        # if photosiz.file_size>fsiz:
        #     fid = photosiz.file_id
        #     fsiz = photosiz.file_size
    # context.bot_data["photo_file_id"] = fid
    # context.bot.send_photo(chat_id, fid)

def audioecho(update, context):
    """Displays infos about a received audio in the console, no user feedback"""

    user_id = update.effective_user.id

    logger.info(f"[audioecho] user {user_id} sent an audio, which characteristics are :")
    logger.info(pformat(update.message.effective_attachment))
    logger.info(f"file_id : {update.message.effective_attachment.file_id}")

def videoecho(update, context):
    """Displays infos about a received video in the console, no user feedback"""

    user_id = update.effective_user.id

    logger.info(f"[videoecho] user {user_id} sent a video, which characteristics are :")
    logger.info(pformat(update.message.effective_attachment))
    logger.info(f"file_id : {update.message.effective_attachment.file_id}")

def docecho(update, context):
    """Displays infos about a received document in the console, no user feedback"""

    user_id = update.effective_user.id

    logger.info(f"[docecho] user {user_id} sent a document, which characteristics are :")
    logger.info(pformat(update.message.effective_attachment))

def locecho(update, context):
    """Displays info about a received location"""
    user_id = update.effective_user.id

    logger.info(f"[locecho] user {user_id} sent a location, which is :")
    logger.info(pformat(update.message.effective_attachment))

def contactecho(update, context):
    """Displays info about a received contact"""
    user_id = update.effective_user.id

    logger.info(f"[contactecho] user {user_id} sent a location, which is :")
    logger.info(pformat(update.message.effective_attachment))
    logger.info(f"phone_number : {update.message.effective_attachment.phone_number}")


def list_command(update, context):
    """
    Donne la liste des activités du jour
    Si le demandeur est staff, donne pour chacune la liste des utilisateurs qui s'y sont inscrit
    """

    chat_id = update.effective_chat.id
    user_id = update.effective_user.id

    users = context.bot_data["users"]
    day = context.bot_data["date"]
    activities_today = activities[day]
    cleared = has_clearance(user_id, CLEAR.STAFF, context)

    for act in activities_today:
        msg = f"--- {act['name']} {act['time']} ---\n\n"
        if cleared:
            i = 1
            for usr in users:
                if act["id"] in users[usr]["registrations"]:
                    msg += f"{i}. {user_id_str(users[usr])}\n"
                    i += 1
        context.bot.send_message(chat_id=chat_id, text=msg)


def list_users(update, context):
    """Get the users list"""

    chat_id = update.effective_chat.id
    user_id = update.effective_user.id

    if has_clearance(user_id, CLEAR.STAFF, context):
        users = context.bot_data["users"]
        logger.info("[/list_users] Current users :")
        logger.info(pformat(context.bot_data["users"]))

        msg = ""
        for rank, usrid in enumerate(users):
            msg += f"{rank+1}. {usrid} {get_user_from_id(context.bot, usrid).name} : {user_id_str(users[usrid])}\n"

            if rank % 10 == 9:
                context.bot.send_message(chat_id=chat_id, text=msg)
                msg = ""

        if msg:
            context.bot.send_message(chat_id=chat_id, text=msg)

    elif context.bot_data["verbose_access_denied"]:
        update.message.reply_text(access_denied)


def broadcast(update, context):
    """Init a broadcasting operation"""
    # ne pas trop en faire, il y a déja le canal officiel

    chat_id = update.effective_chat.id
    user_id = update.effective_user.id

    if has_clearance(user_id, CLEAR.STAFF, context):
        # possibilité d'ajouter un niveau de clearance entre STAFF et ADMIN
        # pour que seuls les respo comm puissent broadcast
        context.user_data["broadcast"] = ""
        context.user_data["selected_users"] = set()
        context.user_data["message"] = ""

        day = context.bot_data["date"]
        activities_today = activities[day]

        keyboard = []
        activities_today_ids = [act["id"] for act in activities_today]
        for act_id in list(subscriptions)+activities_today_ids:
            keyboard.append([InlineKeyboardButton(act_id, callback_data=bdc_prefix1+act_id)])
        keyboard.append([InlineKeyboardButton("Sélectionner manuellement des utilisateurs",
                                              callback_data=bdc_prefix1+"user_select")])
        keyboard.append([InlineKeyboardButton("Envoyer à tout le staff",
                                              callback_data=bdc_prefix1+"staff")])
        if config.wide_broadcast_enabled:
            keyboard.append([InlineKeyboardButton("Envoyer à tout le monde",
                                                  callback_data=bdc_prefix1+"everyone")])
        context.bot.send_message(chat_id=chat_id, text=broadcast_str[BROADCAST.CHANNEL],
                                 reply_markup=InlineKeyboardMarkup(keyboard))
        return BROADCAST.CHANNEL

    if context.bot_data["verbose_access_denied"]:
        context.bot.send_message(chat_id=chat_id, text=access_denied)
    return ConversationHandler.END


def channel(update, context):
    """Select the channel while in a broadcasting operation, using text input, legacy (not even sure it works)"""

    chat_id = update.effective_chat.id
    target = update.message.text.strip()

    day = context.bot_data["date"]
    activities_today = activities[day]

    for act in activities_today:
        if target in (act["id"], "dailies", "enigms"):
            context.user_data["broadcast"] = target
            context.bot.send_message(chat_id=chat_id, text=right_channel.format(target))
            context.bot.send_message(chat_id=chat_id, text=broadcast_str[BROADCAST.MESSAGE])
            return BROADCAST.MESSAGE

    context.bot.send_message(chat_id=chat_id, text=wrong_channel.format(target))
    return BROADCAST.CHANNEL


def bdc_button1(update, context):
    """Select the channel while in broadcasting operation, using InlineButtons"""

    chat_id = update.effective_chat.id
    query = update.callback_query
    target = query.data[4:]

    if "user_select" in target:
        context.user_data["selected_users"] = set()
        keyboard = []
        for userid in context.bot_data["users"]:
            keyboard.append([InlineKeyboardButton(context.bot_data["users"][userid]["pseudo"] + " ❌", callback_data=bdc_prefix2+str(userid))])
        keyboard.append([InlineKeyboardButton("/cancel", callback_data=bdc_prefix2+"end")])
        query.edit_message_text(broadcast_str[BROADCAST.USERS_SELECT], reply_markup=InlineKeyboardMarkup(keyboard))
        return BROADCAST.USERS_SELECT
    context.user_data["broadcast"] = target
    query.edit_message_text(right_channel.format(target))
    context.bot.send_message(chat_id=chat_id, text=broadcast_str[BROADCAST.MESSAGE])
    return BROADCAST.MESSAGE


def bdc_button2(update, context):
    """Select the specific users while in broadcasting operation"""

    query = update.callback_query
    target = query.data[4:]

    if target == "end":
        query.edit_message_text(success_cancel)
        return ConversationHandler.END
    if target == "ok":
        context.user_data["broadcast"] = "selected"
        query.edit_message_text(broadcast_str[BROADCAST.MESSAGE])
        return BROADCAST.MESSAGE

    selected_users = context.user_data["selected_users"]
    target = int(target)
    if target in selected_users:
        selected_users.remove(target)
    else:
        selected_users.add(target)
    context.user_data["selected_users"] = selected_users

    keyboard = []
    for userid in context.bot_data["users"]:
        keyboard.append([InlineKeyboardButton(context.bot_data["users"][userid]["pseudo"] + (" ✅" if (userid in selected_users) else " ❌"), callback_data=bdc_prefix2+str(userid))])
    if selected_users: # si on en a sélectionné
        keyboard.append([InlineKeyboardButton(select_these_users, callback_data=bdc_prefix2+"ok")])
    keyboard.append([InlineKeyboardButton("/cancel", callback_data=bdc_prefix2+"end")])
    query.edit_message_text(broadcast_str[BROADCAST.CHANNEL], reply_markup=InlineKeyboardMarkup(keyboard))
    return BROADCAST.USERS_SELECT


def message(update, context):
    """Receive the message to broadcast and ask for confirmation"""

    chat_id = update.effective_chat.id

    context.user_data["message"] = update.message.text
    context.bot.send_message(chat_id=chat_id, text=broadcast_str[BROADCAST.CONFIRMATION], quote=True)

    return BROADCAST.CONFIRMATION


def confirmation(update, context):
    """Get the confirmation for the current message being broadcast by `user_id`
       and send it the the users subscribed or registered to `target`"""

    chat_id = update.effective_chat.id
    user_id = update.effective_user.id

    answer = update.message.text.strip().lower()

    if answer in ("o", "oui"):
        target = context.user_data["broadcast"]
        msg = context.user_data["message"]

        logger.info(f"[/broadcast] user {user_id} ({update.effective_user.name}) sent the following broadcast to {target} :\n{msg}")

        if "selected" in target:
            users = context.user_data["selected_users"]
            for usr in users:
                context.bot.send_message(chat_id=usr, text=msg)
        else:
            users = context.bot_data["users"]
            for usr in users:
                if (target in users[usr]["registrations"]) or (target in users[usr]["subscriptions"]) or ("everyone" in target) or ("staff" in target and has_clearance(usr, CLEAR.STAFF, context)):
                    context.bot.send_message(chat_id=usr, text=msg)

        context.user_data["broadcast"] = ""
        context.user_data["selected_users"] = ""
        context.user_data["message"] = ""

        context.bot.send_message(chat_id=chat_id, text=broadcast_str[BROADCAST.SENT])

        return ConversationHandler.END

    if answer in ("n", "non"):
        context.bot.send_message(chat_id=chat_id, text=wrong_message)
        return BROADCAST.MESSAGE

    context.bot.send_message(chat_id=chat_id, text=incorrect_yes_no)
    return BROADCAST.CONFIRMATION


def elevate(update, context):
    """Give someone staff powers"""

    user_id = update.effective_user.id

    if has_clearance(user_id, CLEAR.ADMIN, context):
        try:
            userid = int(context.args[0])
        except (IndexError, ValueError):
            update.message.reply_text("Syntaxe : /elevate [userid]")
            return
        if userid in context.bot_data["users"]:
            context.bot_data["users"][userid]["clearance"] = CLEAR.STAFF
            update.message.reply_text(elevated_success.format(str(userid)))
        else:
            update.message.reply_text(user_not_found.format(str(userid)))

    elif context.bot_data["verbose_access_denied"]:
        update.message.reply_text(access_denied)

def demote(update, context):
    """Strip someone from staff powers"""

    user_id = update.effective_user.id

    if has_clearance(user_id, CLEAR.ADMIN, context):
        try:
            userid = int(context.args[0])
        except (IndexError, ValueError):
            update.message.reply_text("Syntaxe : /demote [userid]")
            return
        if userid in context.bot_data["users"]:
            context.bot_data["users"][userid]["clearance"] = CLEAR.NONE
            update.message.reply_text(elevated_success.format(str(userid)))
        else:
            update.message.reply_text(user_not_found.format(str(userid)))

    elif context.bot_data["verbose_access_denied"]:
        update.message.reply_text(access_denied)
