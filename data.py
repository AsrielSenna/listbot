from datetime import datetime, time
from collections import namedtuple

class Enum:
    __init__ = None

class CLEAR(Enum):
    NONE, STAFF, ADMIN = range(3)


# Time reference for delay calculation
# mettre la date de début des campagnes
# pas utilisé pour le moment
time_origin = datetime(2020, 4, 25, 12, 00, 00)


## User data
majors = ("INFO 💚",
          "MMK ❤",
          "ELEC 💙",
          "TELE 💛",
          "RSI 💜",
          "SEE 💗")

subscriptions = {"dailies": "Planning du jour", # reçu chaque jour pour présenter la journée à venir
                 # "enigms": "Jeu des énigmes", # reçu chaque jour pour dire que des nouvelles énigmes sont disponibles
                 "acti_notifs": "Rappel des activités"} # rappel avant chaque activité sans inscription, non implémenté

empty_user = {"pseudo": "",
              "major": "",
              "first_name": "",
              "last_name": "",
              "clearance": CLEAR.NONE,
              "subscriptions": list(subscriptions),
              "registrations": [],
              "enigm_data": {"time": time_origin,
                             "answers": 0}}


## Hello data
ask = ("Ce bot n'a actuellement pas de propriétaire, veux-tu en prendre le contrôle ?",
       "Sous quel pseudo souhaites-tu participer à nos campagnes ?",
       "Super ! Tu étudies dans quelle filière ?",
       "Je le savais, la meilleure !\nQuel est ton prénom ?",
       "Okay, maintenant ton nom de famille ?",
       "Quelles notifications souhaites-tu recevoir ? Tu pourras modifier ce paramètre plus tard avec la commande /subscribe",
       "Génial, merci 😊")
class HELLO(Enum):
    TAKEOVER, PSEUDO, MAJOR, FIRST_NAME, LAST_NAME, SUBSCRIBE, END = range(len(ask))

validation_subscribe = "J'ai bien pris en compte tes inscriptions"
invalid_input = "Entrée invalide 😔 réessaye !"

intro = "Bonjour, je m'appelle @ListBot 🤖 et je m'occupe des inscriptions aux différentes activités organisées par RéVisionn'eirb durant nos campagnes.\nVotez RéVisionn'eirb !"
ask_data = """Je vais commencer par te demander quelques informations qui me seront utiles 😉
Attention, je ne retiens pas plus de 30 caractères !"""
recap_data = "Je récapitule :"
incorrect_data = "S'il y a une erreur, tu peux exécuter la commande /start_again"
already_started = "Je crois qu'on se connaît déjà ! 😏"
inform_cancel = "Pour annuler, tu peux envoyer /cancel"
success_cancel = "Ok, on s'arrête là 😅" # message correspondant au /cancel
access_denied = "Petit curieux 😅"

finish_start = """Tu peux tester les commandes à ta disposition, par exemple pour demander un /allo quand tu le souhaites 😊
Et si jamais tu es bloqué, essaie /help !"""


## activités
register_question = "Voici la liste des activités du jour.\nÀ quelle(s) activité(s) veux-tu t'inscrire ?"
subscribe_validation = "J'ai fini ma sélection"
validation_subscribe = "J'ai bien pris en compte tes inscriptions"

# Activities
kahoot_str = "Kahoot"
pictio_str = "Pictionnary"
blind_str = "Blind-test"
master_str = "Master of the Grid"
champion_str = "Question pour un Champion"

# Probably could do better with proper JSON files but I don't have
# the time to mess around with that
activities = {27: [{"id": "kahoot1", "name": kahoot_str, "time": time(14, 00)},
                   {"id": "blind1",  "name": blind_str,  "time": time(18, 00)},
                   {"id": "master1", "name": master_str, "time": time(21, 00)}],

              28: [{"id": "pictio1", "name": pictio_str, "time": time(10, 30)},
                   {"id": "blind2",  "name": blind_str,  "time": time(14, 00)},
                   {"id": "kahoot2", "name": kahoot_str, "time": time(18, 00)},
                   {"id": "blind3",  "name": blind_str,  "time": time(21, 30)}],

              29: [{"id": "kahoot3", "name": kahoot_str, "time": time(11, 00)},
                   {"id": "kahoot4", "name": kahoot_str, "time": time(16, 00)},
                   {"id": "blind4",  "name": blind_str,  "time": time(18, 00)},
                   {"id": "pictio2", "name": pictio_str, "time": time(21, 30)}],

              30: [{"id": "kahoot5", "name": kahoot_str, "time": time(11, 00)},
                   {"id": "master2", "name": master_str, "time": time(14, 00)},
                   {"id": "blind5",  "name": blind_str,  "time": time(18, 00)},
                   {"id": "kahoot6", "name": kahoot_str, "time": time(21, 30)}],

              31: [{"id": "pictio3",  "name": pictio_str,   "time": time(10, 30)},
                   {"id": "champion", "name": champion_str, "time": time(21, 30)},
                   {"id": "blind6",   "name": blind_str,    "time": time(18, 00)}]}


## Broadcast data
broadcast_str = ("À quelle activité ton message est-il dédié ?",
                 "À quels utilisateurs ton message est-il dédié ?",
                 "Quel est ton message ?",
                 "Okay ce message est-il correct ? [O/N]\nRelis-toi 😉 cette action n'est pas réversible",
                 "C'est parti !")
class BROADCAST(Enum):
    CHANNEL, USERS_SELECT, MESSAGE, CONFIRMATION, SENT = range(len(broadcast_str))

select_these_users = "Sélectionner ces utilisateurs"
right_channel = "Super, un message pour \"{}\" !"
wrong_channel = "Je ne connais pas cette activité : \"{}\"...\nOn recommence ?"

wrong_message = "Mince, on recommence alors...\nQuel est ton messsage ?"
incorrect_yes_no = "Pas compris... [O/N] ?"

planning_next_day = "Le planning des activités vient d'être mis à jour !\nTu peux t'inscrire en effectuant la commande /register"
update_day_finished = "Nous sommes passés au jour {} avec succès"

elevated_success = "Les droits de staff ont été accordés à l'utilisateur {}."
demoted_success = "Les droits de staff ont été retirés à l'utilisateur {}."
user_not_found = "L'utilisateur {} n'a pas été trouvé dans la base de données."

next_day_dailies = "Le planning des activités vient d'être mis à jour !\nTu peux t'inscrire en effectuant la commande /register"
next_day_enigms = "De nouvelles énigmes sont disponibles !\nTu peux commencer à les résoudre en lançant /enigms"

AlloType = namedtuple("AlloType", ('intel', 'group_chat_id'), defaults=(-123456789,))

## Allos

intel_asks = ("Combien en veux-tu ?",
              "Tu les veux à quoi, les crèpes ?",
              "Tu peux me donner ton numéro ?",
              "C'est quoi ton adresse ?",
              "C'est pour aller où ?",
              "C'est pour quand ?",
              "Zone d'expression libre :")
class ALLO(Enum):
    QTY, CREPTASTE, NUMBER, ADDRESS, GOTO, TIME, MESSAGE = range(len(intel_asks))
    TYPE, INTEL = range(2)

allo_groups = {"Dessin 🖼️" : AlloType((ALLO.MESSAGE,)),
               "Alexandrin" : AlloType((ALLO.MESSAGE,)),
               "Musique 🎶" : AlloType((ALLO.MESSAGE,)),
               "Culture"    : AlloType(()),
               "Bonjour 📣" : AlloType((ALLO.MESSAGE,)), # envoyer un message à la liste sans attendre de réponse particulière
               "Question ❓" : AlloType((ALLO.MESSAGE,)), # poser une question, qui attend donc une réponse
               "Crèpes 🥞"  : AlloType((ALLO.QTY, ALLO.CREPTASTE, ALLO.ADDRESS, ALLO.NUMBER, ALLO.MESSAGE)), # commander n crèpes au goùt g à l'adresse a
               "Taxi 🚖"    : AlloType((ALLO.ADDRESS, ALLO.GOTO, ALLO.TIME, ALLO.NUMBER, ALLO.MESSAGE)), # commander un trajet de l'adresse à goto à l'heure donnée
               "Meme 😂"    : AlloType((ALLO.MESSAGE,))}
# énumérer tous les types d'allô, avec à chaque fois l'id d'un groupe telegram où sont tous ceux qui s'occupent du allo
# il est possible de mettre le même groupe pour plusieurs types d'allos différents

creptastes = ["chocolat 🍫", "fraise 🍓", "peu importe"]

allo_welcome = "Quel type de allô souhaites-tu ?\nTu peux annuler à tout moment avec /cancel"
allo_valid_type = "Okay, un allô {}." # " Quelle est ta demande ?"
allo_invalid_type = "Hum, je n'ai pas compris 😅"
new_allo = "Nouvel allô {} de {}, {} :\n\n{}"
allo_sent = "J'ai transmis ta demande 😊"
allos_disabled = "Les allô sont désactivés pour le moment ! Reviens plus tard 🥰"


# /help texts and links
# tutos = "Le Myst'eirb reste entier 🕵️\nEncore un peu de patience !"
tutos = """Fonctionnement de {} :

S'inscrire avec /start et changer ses infos avec /start_again
Faire un allo avec /allo
Tester si le bot est en ligne avec /ping
Le programme de la semaine est sur /week
Découvrir RéVisionn'eirb dans son ensemble avec /team
"""


## présentation de la liste
team_pic = "AgACAgQAAxkBAAIEl16whOFnZzV-o4K1OZShraoqTlphAAJltDEb9F-BUQLpXmGvOtIRkv0hGwAEAQADAgADeQAD0r0HAAEZBA"
# envoyer une image au bot, choper son file_id dans la console avec photoecho et le coller ici
# team_musik = "CQACAgQAAxkBAAIErl6wjsOnTgNpgduQhL7ASf9mn2nIAALrBgACdW0hUxdJctw8u3u0GQQ" # infinity WZ
team_musik = "CQACAgQAAxkBAAIEr16wjyPmUJc4FVet9XIPmNiiKHsSAAJuCAAC9F-BUVyagNgh_Xi2GQQ" # Maniac
team_video = "BAACAgQAAxkBAAIEwV6wlKnksH3d70t2mgxpqkGo4scRAAJ0CAAC9F-BUUYemiWrpXhxGQQ" # Flash
team_prez = """<b><u>La team RéVisionn'eirb :</u></b>

<b><u>- Bureau -</u></b>
<b>Président :</b>
Pierre "Asriel Senna" Pavia
@Gouvernathor

<b>Président d'Honneur :</b>
Victor "Paix et Amour" Chapal
@C_plus_moi_qui_ai_les_clefs

<b>Vice Président :</b>
Flavien "Flavienküche" Maheu
@flavieng

<b>Trésorier :</b>
Tom "Boule Molle" Allio
@trezoduku

<b>Secrétaire :</b>
Pierre "Pedrolito" Andraud
@lautrepierre


<b>Coordinateur :</b>
Valentin "Lumineux" Ramirez
@horsehead


<b><u>- Pôle Comm -</u></b>
<b>Respo :</b>
Kais "Gros Perse" Hadi
@Eirbivore

Floris "Vidéo" Choisy
@peiba


<b><u>- Pôle Partenariat -</u></b>
<b>Respo Externe :</b>
Quentin "Slack" Biechlé
@boblon

<b>Respo Interne :</b>
Florian "Mr Saxobeat" Mornet
@saxobite

"""


## Enigms data
DAILY_NUMBER = 10

empty_enigm_tracker = {27: [False] * DAILY_NUMBER,
                       28: [False] * DAILY_NUMBER,
                       29: [False] * DAILY_NUMBER,
                       30: [False] * DAILY_NUMBER,
                       31: [False] * DAILY_NUMBER}

enigms_answered_daily = "Tu as déjà répondu à toutes les énigmes du jour, bravo ! 👏🏻"

too_late = """Mince, tu es en retard, cette énigme n'est plus disponible... ⌚
Tu peux commencer celles d'aujourd'hui avec /next_enigm"""

well_dones = ("Bien joué !",
              "Oui c'est ça, bravo !",
              "Super !",
              "Génial !",
              "T'es trop fort 💪🏻",
              "Comment t'as deviné ?")

try_agains = ("Essaie encore !",
              "Non, ce n'est pas ça...",
              "C'est facile pourtant 🙂",
              "Non, désolé",
              "Ce n'est pas la bonne réponse",
              "Cherche encore !")

numerals = ("Zéroième ?!",
            "Première",
            "Deuxième",
            "Troisième",
            "Quatrième",
            "Cinquième",
            "Sixième",
            "Septième",
            "Huitième",
            "Neuvième",
            "Dernière")

not_subscribed = "Tu n'es pas inscrit à cette activité 😅 Commence par /subscribe 😊"
explanation_enigms = """Chaque jour, je te propose 10 énigmes à résoudre. Il s'agit de reconnaître un monument célèbre à partir d'une photo, d'un poème...
Pour obtenir la prochaine énigme, lance la commande /next_enigm. Tu peux ensuite répondre directement par chat et j'analyse tes réponses."""
move_on = "Tu peux passer à l'énigme suivante avec /next_enigm si tu le souhaites !"


## Button prefixes
reg_prefix = "reg_" # register
sub_prefix = "sub_" # subscribe
bdc_prefix1 = "bdc_" # broadcast 1
bdc_prefix2 = "ust_" # broadcast 2 (user select)
sho_prefix = "sho_" # prise en charge d'un allo

# Unexpected interaction
lost = "Tu t'es perdu ?"

# used by is_known_user
not_known_user = "Tu n'es pas enregistré, commence par /start 😃"

# réponses au /ping
fun_answers = ("Bip boup. Boup bip ? 🤖",
               "Hello, world!",
               "Beep Boop. Boop Beep ?",
               "Votez RéVisionn'eirb ! 💜",
               "Certes. Mais que faire de cette information ?",
               "Oui, je suis connecté 😁",
               "Bop bilibopbop",
               "?",
               "😏",
               "👾")
